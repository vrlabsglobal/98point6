package com.vishwatej.point6.Models;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "games")
public class Games {
	
	@Id
	private String _id;
	private String state;
	private String winner;
	private List<String> players;
	private int rows;
	private int columns;
	private int[][] game;
	private List<Move> moves;
	private String turn;
	private int[] colsize;

	public String getTurn() {
		return turn;
	}

	public void setTurn(String turn) {
		this.turn = turn;
	}

	public List<Move> getMoves() {
		return moves;
	}

	public void setMoves(List<Move> moves) {
		this.moves = moves;
	}

	public Games(List<String> players, int rows, int columns) {
		super();
		this.state = "IN_PROGRESS";
		this.players = players;
		this.rows = rows;
		this.columns = columns;
		this.game = new int[rows][columns];
		this.moves = new LinkedList<Move>();
		this.colsize = new int[columns];
		Arrays.fill(this.colsize, rows-1);
		this.turn = "";
		this.winner = "";
	}

	public int[] getColsize() {
		return colsize;
	}

	public void setColsize(int[] colsize) {
		this.colsize = colsize;
	}

	public int[][] getGame() {
		return game;
	}

	public void setGame(int[][] game) {
		this.game = game;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getWinner() {
		return winner;
	}

	public void setWinner(String winner) {
		this.winner = winner;
	}

	public List<String> getPlayers() {
		return players;
	}

	public void setPlayers(List<String> players) {
		this.players = players;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

}
