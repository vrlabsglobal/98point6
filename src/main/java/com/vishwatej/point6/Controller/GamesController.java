package com.vishwatej.point6.Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vishwatej.point6.Models.Games;
import com.vishwatej.point6.Models.Move;
import com.vishwatej.point6.Repositories.GamesRepository;

@RestController
@RequestMapping("/drop_token")
public class GamesController {
	
	@Autowired
	private GamesRepository repository;
	
	@RequestMapping(method = RequestMethod.POST)
	public Map<String,Object> createGame(@Valid @RequestBody Games game, HttpServletResponse res) {
		if(game.getPlayers().size() != 2) {
			res.setStatus( HttpServletResponse.SC_BAD_REQUEST);
			res.setStatus( HttpServletResponse.SC_NOT_FOUND);
			Map<String, Object> gameMap = new HashMap<String, Object>();
			gameMap.put("msg", "There should be exactly two players in the game");
			return gameMap;
		}
		repository.save(game);
		Map<String, Object> gameMap = new HashMap<String, Object>();
		gameMap.put("gameId", game.get_id());
		return gameMap;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public List<String> getActiveGames() {
	  return repository.findByTheGameByState("IN_PROGRESS");
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> getGameStatus(@PathVariable String id, HttpServletResponse res) {
	  Games game = repository.findBy_id(id);
	  if(game == null) {
		  res.setStatus( HttpServletResponse.SC_NOT_FOUND);
		  Map<String, Object> gameMap = new HashMap<String, Object>();
		  gameMap.put("msg", "The game with the given id does not exist");
		  return gameMap;
	  }
	  Map<String, Object> gameMap = new HashMap<String, Object>();
	  gameMap.put("players", game.getPlayers());
	  gameMap.put("state", game.getState());
	  if(game.getState().equals("DONE")) if(!game.getWinner().equals("")) gameMap.put("winner", game.getWinner());
	  return gameMap;
	}
	
	@RequestMapping(value="/{game_id}/{player_id}", method = RequestMethod.POST)
	public Map<String, Object> postMove(@PathVariable String game_id, @PathVariable String player_id, HttpServletResponse res, @Valid @RequestBody Map<String,Object> body) {
		Games game = repository.findBy_id(game_id);
		if(game == null) {
			res.setStatus( HttpServletResponse.SC_NOT_FOUND);
			Map<String, Object> gameMap = new HashMap<String, Object>();
			gameMap.put("msg", "The game with the given id does not exist");
			return gameMap;
		}
		if(!game.getPlayers().contains(player_id)) {
			res.setStatus( HttpServletResponse.SC_NOT_FOUND);
			Map<String, Object> gameMap = new HashMap<String, Object>();
			gameMap.put("msg", "You are not a part of the game");
			return gameMap;
		}
		if(!game.getWinner().equals("")) {
			res.setStatus( HttpServletResponse.SC_BAD_REQUEST);
			Map<String, Object> gameMap = new HashMap<String, Object>();
			gameMap.put("msg", "This game has ended and the winner is " + game.getWinner());
			return gameMap;
		}
		if(game.getTurn().equals(player_id)) {
			res.setStatus( HttpServletResponse.SC_CONFLICT);
			Map<String, Object> gameMap = new HashMap<String, Object>();
			gameMap.put("msg", "This is not your turn");
			return gameMap;
		}
		if(!(body.get("column") instanceof Integer)) {
			res.setStatus( HttpServletResponse.SC_BAD_REQUEST);
			Map<String, Object> gameMap = new HashMap<String, Object>();
			gameMap.put("msg", "Invalid move. Please enter a valid move");
			return gameMap;
		}
		int col = (int) body.get("column");
		if(col > game.getColumns() || col <= 0) {
			res.setStatus( HttpServletResponse.SC_BAD_REQUEST);
			Map<String, Object> gameMap = new HashMap<String, Object>();
			gameMap.put("msg", "Invalid move. Please enter a valid move");
			return gameMap;
		}
		Move move = new Move();
		move.setColumn(col);
		move.setPlayer(player_id);
		move.setType("MOVE");
		List<Move> moves = game.getMoves();
		moves.add(move);
		game.setMoves(moves);
		int[][] new_state = game.getGame();
		int[] colsize = game.getColsize();
//		System.out.println(player_id.equals("player1"));
		if(colsize[col-1] < 0) {
			res.setStatus( HttpServletResponse.SC_BAD_REQUEST);
			Map<String, Object> gameMap = new HashMap<String, Object>();
			gameMap.put("msg", "The entered column is already filled. Please enter a different column number");
			return gameMap;
		}
		new_state[colsize[col-1]][col-1] = (player_id.equals("player1") ? 1 : 2);
		colsize[col-1] -= 1;
		
		// checks along the column to see if the player has won
		boolean win = false;
		for(int i=0;i<game.getRows();i++) {
			int count = 1;
			for(int j=0;j<game.getColumns()-1;j++) {
				if(new_state[i][j] == (player_id.equals("player1") ? 1 : 2)  && new_state[i][j] == new_state[i][j+1]) count += 1;
			}
			if(count == game.getColumns()) win = true;
			if(win) {
				game.setState("DONE");
				game.setWinner(player_id);
				repository.save(game);
				res.setStatus( HttpServletResponse.SC_ACCEPTED);
				Map<String, Object> gameMap = new HashMap<String, Object>();
				gameMap.put("msg", player_id + " has won the game");
				return gameMap;
			}
		}
		
		// checks along the row to see if the player has won
		if(!win) {
			for(int j=0;j<game.getColumns();j++) {
				int count = 1;
				for(int i=0;i<game.getRows()-1;i++) {
					if(new_state[i][j] == (player_id.equals("player1") ? 1 : 2) && new_state[i][j] == new_state[i+1][j]) count += 1;
				}
				if(count == game.getRows()) win = true;
				if(win) {
					game.setState("DONE");
					game.setWinner(player_id);
					repository.save(game);
					res.setStatus( HttpServletResponse.SC_ACCEPTED);
					Map<String, Object> gameMap = new HashMap<String, Object>();
					gameMap.put("msg", player_id + " has won the game");
					return gameMap;
				}
			}
		}
		
		//checks along the anti-diagonal to see if the player has won
		if(!win) {
			int count = 1;
			for(int i=0;i<game.getRows()-1;i++) {
				for(int j=0;j<game.getRows()-1;j++) {
					if(i + j == game.getColumns()-2) 
					if(new_state[i][j+1] == (player_id.equals("player1") ? 1 : 2) && new_state[i][j+1] == new_state[i+1][j]) count += 1;
				}
			}
			if(count == game.getColumns()) win = true;
			if(win) {
				game.setState("DONE");
				game.setWinner(player_id);
				repository.save(game);
				res.setStatus( HttpServletResponse.SC_ACCEPTED);
				Map<String, Object> gameMap = new HashMap<String, Object>();
				gameMap.put("msg", player_id + " has won the game");
				return gameMap;
			}
		}
		
		//checks along the diagonal to see if the player has won.
		if(!win) {
			int count = 1;
			for(int i=0;i<game.getRows()-1;i++) {
				for(int j=0;j<game.getColumns()-1;j++) {
					if(i == j) 
					if(new_state[i][j] == (player_id.equals("player1") ? 1 : 2) && new_state[i][j] == new_state[i+1][j+1]) count += 1;
				}
			}
			if(count == game.getColumns()) win = true;
			if(win) {
				game.setState("DONE");
				game.setWinner(player_id);
				repository.save(game);
				res.setStatus( HttpServletResponse.SC_ACCEPTED);
				Map<String, Object> gameMap = new HashMap<String, Object>();
				gameMap.put("msg", player_id + " has won the game");
				return gameMap;
			}
		}
//		if(!win) {
//			int count = 1;
//			for(int i=0;i<game.getRows()-1;i++) {
//				for(int j=0;j<game.getColumns()-1;j++) {
//					if(i + j == game.getColumns()) 
//					if(new_state[i][j] == (player_id.equals("player1") ? 1 : 2) && new_state[i][j] == new_state[i-1][j-1]) count += 1;
//				}
//			}
//			if(count == game.getColumns()) win = true;
//			if(win) {
//				game.setState("DONE");
//				game.setWinner(player_id);
//				repository.save(game);
//				res.setStatus( HttpServletResponse.SC_ACCEPTED);
//				Map<String, Object> gameMap = new HashMap<String, Object>();
//				gameMap.put("msg", player_id + " has won the game");
//				return gameMap;
//			}
//		}
		
		// checks if the current game is a draw or does it still have a chance of moving forward
		int count = 0;
		for(int i = 0; i<game.getRows();i++) {
			for(int j=0; j<game.getColumns();j++) {
				if(new_state[i][j] != 0) count += 1;
			}
		}
		if(count == game.getRows()*game.getColumns()) {
			game.setState("DONE");
			repository.save(game);
			res.setStatus( HttpServletResponse.SC_ACCEPTED);
			Map<String, Object> gameMap = new HashMap<String, Object>();
			gameMap.put("msg", "This game is a draw and nobody wins");
			return gameMap;
		}
		game.setGame(new_state);
		game.setColsize(colsize);
		game.setTurn(player_id);
		repository.save(game);
		res.setStatus( HttpServletResponse.SC_ACCEPTED);
		for(int i=0; i<4;i++) {
			for(int j=0; j<4; j++) {
				System.out.print(new_state[i][j]);
			}
			System.out.println();
		}
		Map<String, Object> gameMap = new HashMap<String, Object>();
		gameMap.put("move", game_id + " /moves/ " + game.getMoves().size());
		return gameMap;
	}
	
	@RequestMapping(value="/{game_id}/moves", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> getMoves(@PathVariable String game_id, HttpServletResponse res) {
	  Games game = repository.findBy_id(game_id);
	  if(game == null) {
		  res.setStatus( HttpServletResponse.SC_NOT_FOUND);
		  Map<String, Object> gameMap = new HashMap<String, Object>();
		  gameMap.put("msg", "The game with the given id does not exist");
		  return gameMap;
	  }
	  Map<String, Object> gameMap = new HashMap<String, Object>();
	  gameMap.put("moves", game.getMoves());
	  return gameMap;
	}
	
	@RequestMapping(value="/{game_id}/moves/{move_num}", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> getMove(@PathVariable String game_id, @PathVariable int move_num, HttpServletResponse res) {
	  Games game = repository.findBy_id(game_id);
	  if(game == null) {
		  res.setStatus( HttpServletResponse.SC_NOT_FOUND);
		  Map<String, Object> gameMap = new HashMap<String, Object>();
		  gameMap.put("msg", "The game with the given id does not exist");
		  return gameMap;
	  }
	  if(move_num > game.getMoves().size()) {
		  res.setStatus( HttpServletResponse.SC_NOT_FOUND);
		  Map<String, Object> gameMap = new HashMap<String, Object>();
		  gameMap.put("msg", "The move number is greater than the available moves");
		  return gameMap;
	  }
	  Map<String, Object> gameMap = new HashMap<String, Object>();
	  gameMap.put("moves", game.getMoves().get(move_num-1));
	  return gameMap;
	}
	
	@RequestMapping(value="/{game_id}/{player_id}", method = RequestMethod.DELETE)
	@ResponseBody
	public Map<String, Object> deleteMove(@PathVariable String game_id, @PathVariable String player_id, HttpServletResponse res) {
		  Games game = repository.findBy_id(game_id);
		  if(game == null) {
			  res.setStatus( HttpServletResponse.SC_NOT_FOUND);
			  Map<String, Object> gameMap = new HashMap<String, Object>();
			  gameMap.put("msg", "The game with the given id does not exist");
			  return gameMap;
		  }
		  if(!game.getPlayers().contains(player_id)) {
			  res.setStatus( HttpServletResponse.SC_NOT_FOUND);
			  Map<String, Object> gameMap = new HashMap<String, Object>();
			  gameMap.put("msg", "You are not a part of the game");
			  return gameMap;
		  }
		  if(game.getState().equals("DONE")) {
			  res.setStatus( HttpServletResponse.SC_GONE);
			  Map<String, Object> gameMap = new HashMap<String, Object>();
			  gameMap.put("msg", "Game is already completed");
			  return gameMap;
		  }
		  Move move = new Move();
		  move.setType("QUIT");
		  move.setPlayer(player_id);
		  List<Move> m = game.getMoves();
		  m.add(move);
		  game.setMoves(m);
		  game.setWinner(player_id.equals("player1")?"player2":"player1");
		  game.setState("DONE");
		  repository.save(game);
		  Map<String,Object> gameMap = new HashMap<String,Object>();
		  return gameMap;
		}
	
}
