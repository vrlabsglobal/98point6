package com.vishwatej.point6.Repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.vishwatej.point6.Models.Games;

public interface GamesRepository extends MongoRepository<Games, String>{
	 @Query(value="{ 'state' : ?0 }", fields="{ '_id' : 1}")
	 List<String> findByTheGameByState(String state);
	 
	 Games findBy_id(String _id);
}
