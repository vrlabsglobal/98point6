package com.vishwatej.point6.Repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.vishwatej.point6.Models.Move;

public interface MoveRepository extends MongoRepository<Move, String>{

}
