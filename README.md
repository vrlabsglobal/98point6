The current REST API has been implemented using Java 8, Mongo DB 3+ version (latest one) and Spring Boot (latest Version)

You will have to download the Tomcat server inorder to run this API on the server. I have tested it on Tomcat server version 9

I have tested the REST API using the windows platform but I belive it should work on any. Kindly download and install the above softwares accroding to your operating system
but be specific with the versions.

After you download and install the softwares required, create a database named drop_token inside mongo db database. This can be done either using the cmd or the Robo 3T user interface (easier one)
By default the mongo DB server runs on localhost 27017, and I have configured my app to connect on that port. Should you decide to run in on different port, kindly make changes accordingly in the 
resources/application.properties file.

Now use your favourite ide to run the tomcat server and lauch the application. The you can use postman to test the api end points.

I came up as many corner cases as possible and tried to make the solution to look as neat as possible and hope you like the solution.